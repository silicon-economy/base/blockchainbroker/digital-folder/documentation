= Chapter 3: System Scope and Context

*Contents*

* <<3.1 Business Context>>
* <<3.2 Technical Context>>

This section describes the environment and scope of the Blockchain Broker.
Who are its users, and with which other systems does it interact.
It thereby specifies the external interfaces (domain interfaces and technical interfaces to communication partners).
The Blockchain Broker is a black box here.

== 3.1 Business Context

Stakeholders of the Blockchain Broker need to understand which data is exchanged with the environment.
All _communication_ partners (users, IT-systems, etc.) of the Blockchain Broker are described below.
It specifies (the interfaces of) all communication partners in business/domain view.
The following table explains the domain-specific inputs and outputs or interfaces, which may include domain-specific formats or communication protocols.

[cols=",,,,,",options="header",]
|===
|Interface
|Kind
|Domain Inputs
|Domain Outputs
|Protocol
|Data format

|SE Services
|IT system
|Use case data
|Use case data
|REST/HTTP
|JSON

|IoT Broker
|IT system
|Data from IoT-Devices
|-
|REST/HTTP
|JSON

|IDS DataSpace Connector
|IT system
|Use case data
|Use case data
|REST/HTTP
|JSON
|===

== 3.2 Technical Context

In contrast to the more abstract business context, here the technical view is described, e.g. different technical channels, protocols or interfaces used to realize the communication with the objects from the business context.
Actual _instances_ of building blocks communicate in the technical context.
For details see link:chapter7.adoc[Chapter 7: Deployment View].

The following technical interfaces link the Blockchain Broker to its environment:

[cols=",,,,",options="header",]
|===
|Interface
|Inputs
|Outputs
|Protocol
|Data format

|IDS Interface
|IDS Messages
|IDS Messages
|REST/HTTP
|IDS Protocol

|Light Node Service
|Transaction Data
|Transaction ID
|gRPC
|JSON
|===

