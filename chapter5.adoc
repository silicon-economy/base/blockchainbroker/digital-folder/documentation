= Chapter 5: Building Block View

*Contents*

* <<5.1 Token Manager>>
** <<5.1.1 Overview>>
* <<5.2 Whitebox - Level 1>>
* <<5.3 Module (Whitebox)>>
** <<5.3.1 Building Blocks - Level 2>>
** <<5.3.2 Module (Blackbox)>>
*** <<5.3.2.1 Keeper>>
*** <<5.3.2.2 Msg Service>>
*** <<5.3.2.3 Query service>>
*** <<5.3.2.4 Interfaces>>
* <<5.4 Building Blocks - Level 3>>
** <<5.4.1 BusinessLogic>>
** <<5.4.2 TokenWallet>>
** <<5.4.3 Token>>
** <<5.4.4 HashToken>>
*** <<5.4.4.1 Transactions>>
** <<5.4.5 Authorization>>

The building block view is a static and hierarchical decomposition of the system into building blocks and their relationships.
It is a collection of black box and white box descriptions for all important components.
Building blocks may be modules, components, subsystems, classes, interfaces, packages, libraries, frameworks, layers, partitions, tiers, functions, macros, operations, data structures, etc.).

In blackbox view, only the _interface_ of the component is visible, not its internals.
However, for interface specifications including all method signatures and their descriptions we refer to the _OpenAPI_ documents.
Only other kinds of interfaces are described here.
This is a simplification of arc42 (which requires short description of interfaces), to avoid redundancy and inconsistencies, and to reduce documentation efforts.

== 5.1 Token Manager

=== 5.1.1 Overview

The TokenManager is a public Proof-of-Stake blockchain built on the Cosmos SDK.
It's primary goal is to link TokenWallets with application-specific Token modules.
The TokenWallet handles the management and representation of physical and digital assets grouped into segments inside a wallet.
The Token module on the other hand, handles the information and metadata of tokens.
The Token Manager enables the creation of tokens, the assignment to wallets or the reallocation of tokens.

The authorization module enables the control of access rights to data and smart contracts using application roles.
Blockchain transactions can be linked to these application roles, which are required to execute the transaction.
Unauthorized transactions are blocked by the blockchain and will not be executed.

== 5.2 Whitebox - Level 1

In level 1, the components of Token Manager and their relationships are shown.

image:drawio_diagrams/token-manager-simplified-architecture.drawio.svg[]

There are the following components with its responsibilities:

[cols=",",options="header",]
|===
|Name
|Responsibility / Description

|Application
|The application implements the Application Blockchain Interface (ABCI), for the state-machine to communicate with the underlying consensus engine (e.g.) Tendermint.
It routes messages and queries to the appropriate module.

|Tendermint
|Provides consensus and networking mechanisms

|Modules
|The modules implement the business logic of their application.
In each module there are message types defined with appropriate handlers which update the main state-machine.

|Auth
|Specifies base transaction types and account types

|Bank
|Handles multi-asset coin transfers between regular accounts and tracks pseudo-transfers with vesting accounts

|TokenWallet
|Handles the representation of physical and digital corporate assets by the aid of tokens.

|Token
|Handles the information and metadata handling of tokens.

|HashToken
|The HashToken is an application specific token implementation for the digital consignment note.
The Token represents the authenticity of a document by storing its hash value.
By storing only its hash the document can be handled privately and still be checked for subsequent changes.

|BusinessLogic
|The BusinessLogic is use case specific and handles the functional interaction between the TokenWallet module and the specific Token modules.

|Authorization
|The Authorization module is a custom module which handles the different application roles and their state.
Different application roles can be managed and stored in the blockchain.
|===

To execute transactions on the chain, the user needs to sign the transactions.
This could either be done with a LightClient on the User's frontend, or using a microservice, so that the signing can be centralized in the Backend.

For each TokenManager component we give a brief overview as a blackbox view.
Each module is implemented with the use of Cosmos SDK.
For more details see <<5.3.1 Building Blocks - Level 2, Level 2>> (whitebox views of the components).

== 5.3 Module (Whitebox)

=== 5.3.1 Building Blocks - Level 2

In level 2, the black boxes of level 1 become white boxes.
We look into each module component in detail.

Main Components of Cosmos SDK Modules

image:drawio_diagrams/main-components-of-sdk-modules.drawio.svg[]

Modules are by convention defined in the ./x/ sub folder (e.g. the bank module will be defined in the ./x/bank folder).
They generally share the same core components:

* A https://docs.cosmos.network/v0.42/building-modules/keeper.html[[.underline]#keeper#] , used to access the module's store(s) and update the state.
* A https://docs.cosmos.network/v0.42/building-modules/messages-and-queries.html#messages[[.underline]#Msg# service]  used to process messages when they are routed to the module by  https://docs.cosmos.network/v0.42/core/baseapp.html#message-routing[[.underline]#BaseApp#]  and trigger state-transitions.
* A https://docs.cosmos.network/v0.42/building-modules/query-services.html[query service], used to process user queries when they are routed to the module by  https://docs.cosmos.network/v0.42/core/baseapp.html#query-routing[[.underline]#BaseApp#] .
* Interfaces, for end users to query the subset of the state defined by the module and create messages of the custom types defined in the module.

For more information visit: https://docs.cosmos.network/v0.42/building-modules/intro.html#main-components-of-sdk-modules[[.underline]#https://docs.cosmos.network/v0.42/building-modules/intro.html#main-components-of-sdk-modules#]

In general there are some production-grade modules that are provided by the Cosmos SDK to be used for Cosmos SDK applications.
Those are for example the Auth and Bank Module.

[cols=",",options="header",]
|===
|Component
|Description

|Auth
|Authentication of accounts and transactions for Cosmos SDK application.

|Bank
|Token (Coin) transfer functionalities.
|===

This section describes the component 1 architecture and its implementation.

=== 5.3.2 Module (Blackbox)

==== 5.3.2.1 Keeper

A keeper, used to access the module's store(s) and update the state.

Keepers refer to a Cosmos SDK abstraction whose role is to manage access to the subset of the state defined by various modules.
Keepers are module-specific, i.e. the subset of state defined by a module can only be accessed by a keeper defined in said module.
If a module needs to access the subset of state defined by another module, a reference to the second module's internal keeper needs to be passed to the first one.
This is done in app.go during the instantiation of module keepers.

==== 5.3.2.2 Msg Service

A Msg service used to process messages when they are routed to the module by BaseApp and trigger state-transitions.

A Msg Service processes  https://docs.cosmos.network/v0.42/building-modules/messages-and-queries.html#messages[messages] . Msg Services are specific to the module in which they are defined, and only process messages defined within the said module.
They are called from BaseApp during  https://docs.cosmos.network/v0.42/core/baseapp.html#delivertx[[.underline]#DeliverTx#] .

==== 5.3.2.3 Query service

A query service, used to process user queries when they are routed to the module by BaseApp.

A query service processes  https://docs.cosmos.network/v0.42/building-modules/messages-and-queries.html#queries[[.underline]#queries#] . Query services are specific to the module in which they are defined, and only process queries defined within said module.
They are called from BaseApp's  https://docs.cosmos.network/v0.42/core/baseapp.html#query[[.underline]#Query# method].

==== 5.3.2.4 Interfaces

Interfaces for end users to query the subset of the state defined by the module and create messages of the custom types defined in the module.

Cosmos SDK modules need to implement the  https://docs.cosmos.network/v0.42/building-modules/module-manager.html#application-module-interfaces[[.underline]#AppModule# interfaces], in order to be managed by the application's  https://docs.cosmos.network/v0.42/building-modules/module-manager.html#module-manager[module manager].
The module manager plays an important role in  https://docs.cosmos.network/v0.42/core/baseapp.html#routing[[.underline]#message# and [.underline]#query# routing], and allows application developers to set the order of execution of a variety of functions like  https://docs.cosmos.network/v0.42/basics/app-anatomy.html#begingblocker-and-endblocker[[.underline]#BeginBlocker# and [.underline]#EndBlocker#] .

== 5.4 Building Blocks - Level 3

We detail the custom module blackbox components occurring on Level 1 & 2, which are developed within this project (i.e. not library components or components developed in other SE projects).
In the following are class diagrams being shown to give an overview of all classes and functions.

=== 5.4.1 BusinessLogic

image:pictures/BusinessLogicTypes.png[]
image:pictures/BusinessLogicKeeper.png[]

The BusinessLogic module is a custom SDK Module.
It has no custom types with module stores, as it only coordinates and handles transaction and query calls to the TokenWallet and Token module.

=== 5.4.2 TokenWallet

image:pictures/TokenWalletTypes.png[]
image:pictures/TokenWalletKeeper.png[]

The Token Wallet is a custom Cosmos SDK Module with its own module stores.
And the architecture components listed in Level 3. In the following the custom types saved in the data store are listed.

After showing the overview of classes in the TokenWallet Module, we will take a look at the most used classes in the module.

image:drawio_diagrams/tokenWallet.drawio.svg[]

Before we create our first token, a TokenWallet has to exist.
A TokenWallet is identified by an id.
The id is an uuid generated by the blockchain when creating a TokenWallet.

The timestamp is a time string formatted in RFC3339 and is being set by the blockchain when creating or updating the object.
A TokenWallet can have multiple Segments but those are only being referenced by the Ids.

image:drawio_diagrams/segment.drawio.svg[]

A Segment is an additional level of aggregation of Tokens inside a TokenWallet.
The id and timestamp are being set by the blockchain when creating or updating a segment object.
The Tokens are not being saved directly in a segment.
They are being referenced by a TokenRef object.
A TokenRef object has the id of the token and a valid status.
The Id of a TokenRef object can be used to retrieve an actual token.
But there are different kinds of tokens inside different token modules with its own KV-Store each.
To know where to use the id in order to retrieve a token, the attribute moduleRef is being used.

image:drawio_diagrams/tokenWalletHistory.drawio.svg[]

The TokenWalletHistory stores the history of a TokenWallet.
One TokenWalletHistory object references one TokenWallet.
It has the same Id as related TokenWallet.
Everytime a TokenWallet is being updated the blockchain saves the former state as a snapshot in a history array inside the TokenWalletHistory.

image:drawio_diagrams/segmentHistory.drawio.svg[]

The SegmentHistory stores the history of a Segment.
One SegmentHistory object references one Segment.
It has the same Id as related Segment.
Everytime a Segment is being updated the blockchain saves the former state as a snapshot in a history array inside the SegmentHistory.

image:drawio_diagrams/configuration.drawio.svg[]

The Configuration is an object to store configuration related things.
At the current time there is one attribute "createHistory".
If it is set to false then there will be no history entries being generated when updating a TokenWallet or Segment.

image:drawio_diagrams/tokenHistoryGlobal.drawio.svg[]

The TokenHistoryGlobal class tracks the history of assignments of a Token to each TokenWallet.

The following table gives an overview and summary of all important types.

[cols=",",options="header",]
|===
|Custom Type
|Description

|TokenWallet
|Enterprise Wallet to represent and organize segments and blockchain accounts.

|TokenWalletAccount
|Blockchain Account Address with an active bool flag.
To link active Blockchain Accounts with eWallets.

|Segment
|A segment belongs to one TokenWallet and organizes token references.

|TokenRef
|A token reference with a valid flag to show if It's valid or not.
The moduleRef string is a module.key.
The module key represents the name of a module with its module store.
The id of the TokenRef can be used inside that module store to get a token.

|TokenWalletHistory
|Saves the TokenWallet states inside an array.

|SegmentHistory
|Saves the segment states inside an array.

|Configuration
|Generated and set inside the genesis block and immutable.
The configuration saves the settings to be used inside the blockchain.
The createHistory flag represents the history setting.
If it's false there will be no history entries inside TokenWalletHistory and segmentHistory.

|TokenHistoryGlobal
|Saves all token - TokenWallet history pairs.
|===

=== 5.4.3 Token

image:pictures/TokenTypes.png[]
image:pictures/TokenKeeper.png[]

The Token Module is a custom Cosmos SDK Module with its own module stores.
And the architecture components listed in Level 2. In the following the custom types saved in the data store are listed.

After showing the overview of classes in the TokenWallet Module, we will take a look at the most used classes in the module.

image:drawio_diagrams/token.drawio.svg[]

Then Token object contains all meta information such as segmentId, timestamp, tokenType and so on.
The Info attribute of Token contains the actual Token information.
In this case the info struct only contains an attribute data of type string because it's the simplest kind of token.
Another implementation of a token would have a more detailed info struct at this point.

image:drawio_diagrams/tokenHistory.drawio.svg[]

The TokenHistory stores the history of a Token.
One TokenHistory object references one Token.
It has the same Id as related Token.
Everytime a Token is being updated the blockchain saves the former state as a snapshot in a history array inside the TokenHistory.

In the following table an overview of all important types and structs are given.

[cols=",",options="header",]
|===
|Custom Type
|Description

|Token
|The Token Type represents real or digital assets with metadata.

|Info (Struct)
|Application specific info struct to save additional information, like document information.

|TokenHistory
|Saves the token states inside an array.
|===

=== 5.4.4 HashToken

image:pictures/HashTokenTypes.png[]
image:pictures/HashTokenKeeper.png[]

The Hash Token Module is a custom Cosmos SDK Module with its own module stores.
The following custom types save in the data store are listed.

image:drawio_diagrams/hashToken-type.drawio.svg[]

==== 5.4.4.1 Transactions

To store a new hash token or update an existing one, use the message MsgCreateHashToken

image:drawio_diagrams/msgCreateHashToken.drawio.svg[]

Writing and updating a document's hash uses the same transaction.
While updating, the old hash value will be stored in the token's history and the old hash value will be stored in the token.

[cols=",,,,",options="header",]
|===
|Description
|MsgInterface
|Parameter
|Return Type
|Return Data

|Query of a document's hash
|MsgFetchDocumentHash()
|id: DocumentId
|JSON-Object
|QueryGetDocumentHashResponse

|Query of a document's hash history
|MsgFetchDocumentHashHistory()
|id:DocumentId
|JSON-Object
|QueryGetDocumentHashHistoryResponse
|===

image:drawio_diagrams/queryGetDocumentHashResponse.drawio.svg[]

=== 5.4.5 Authorization

image:pictures/AuthorizationTypes.png[]
image:pictures/AuthorizationKeeper.png[]

The authorization module can be used to manage application roles.
It can create new roles and access or update these roles.
Typically, another custom module will check the authorization module for the validity of an application role and use the result in its own module.

image:drawio_diagrams/applicationRole.drawio.svg[]

image:drawio_diagrams/blockchainAccount.drawio.svg[]

==== Transactions

To assign an application role to a blockchain account, use the message MsgGrantAppRoleToBlockchainAccount

image:drawio_diagrams/msgGrantAppRoleToBlockchainAccount.drawio.svg[]

To revoke an application role from a blockchain account, use the message MsgRevokeAppRoleFromBlockchainAccount

image:drawio_diagrams/msgRevokeAppRoleFromBlockchainAccount.drawio.svg[]

[cols=",,,,",options="header",]
|===
|Description
|MsgInterface
|Parameter
|Return Type
|Return Data

|Grant an applicationRole
|MsgGrantAppRoleToBlockchainAccount
|user: BlockchainAccount

roleID: ApplicationRole
|JSON-Object
|MsgGrantAppRoleToBlockchainAccountResponse (Empty Response)

|Revoke an applicationRole
|MsgRevokeAppRoleFromBlockchainAccount
|account: BlockchainAccount

roleID: ApplicationRole
|JSON-Object
|MsgRevokeAppRoleFromBlockchainAccountResponse (Empty Response)
|===

