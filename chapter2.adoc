= Chapter 2: Architecture Constraints

*Contents*

* <<2.1 Technical Constraints>>
* <<2.2 Organizational Constraints>>
* <<2.3 Political Constraints>>
* <<2.4 Conventions>>

Various constraints are respected for the design of the Blockchain Broker.
They all affect the solution.
We list these restrictions and explain their motivations.
Some of these constraints go beyond an individual Blockchain Broker and are valid for the whole platform or organization.

== 2.1 Technical Constraints

The following technical constraints apply:

[cols=",,",options="header",]
|===
|Id
|Constraint
|Value/Description

|*BAP1*
|Basic Architecture Patterns
|Microservices

|*BAP2*
|Basic Architecture Patterns
|Every Microservice is executed in an application container

|*BAP3*
|Basic Architecture Patterns
|Loose coupling

|*BAP4*
|Basic Architecture Patterns
|Event-based communication (when appropriate)

|*TE1*
|Target Environment
|OKD/Kubernetes (basic stack of a Silicon Economy Platform)

|*BSD1*
|Basic Stack for Development: Application Container
|Docker

|*BSD2*
|Basic Stack for Development: Interfaces/Communication
|HTTP/REST, MQTT, AMQP, WebSockets

|*BSD3*
|Basic Stack for Development: programming languages
|Programming Languages: TypesScript, Go

|*BSD4*
|Basic Stack for Development: other
|All other technologies must be compatible with OKD/Kubernetes

|*BC1*
|Continuous Integration Pipeline Constraints
|Integrated with the GitLab CI Runner

|*BC2*
|Project License
|Developed under the Open Logistics Foundation License 1.3

|*BC3*
|Requirements at Libraries
|Only libraries under a Copy-Left free Open Source License can be used. Accepted open source licenses are, among others, *Apache License 2.0, MIT License, BSD License*

|*BC4*
|Libraries Constraints
|Frameworks & libraries under an open-source license (such as GPL, LGPL) are restricted by the use of the Open Logistics Foundation License 1.3
|===

== 2.2 Organizational Constraints

The following organizational constraints apply:

[cols=",,",options="header",]
|===
|Id
|Constraint
|Background and / or Motivation

|*O1*
|*Schedule*
a|
Milestones: +
09/2020 start of development +
03/2021 completion of version 1.0 of the Token Manager +
03/2021 start of development for the Silicon Economy +
05/2021 first running prototype of the Token Manager for the Silicon Economy +
06/2021 second running prototype of the Token Manager for the Silicon Economy +
10/2021 third running prototype of the Token Manager for the Silicon Economy +
09/2021 completion of version 1.0 of the IDS-BC-Connector +
10/2021 completion of version 1.0 of the IoT-BC-Connector +
11/2021 first running prototype of the IoT-BC-Connector for the Silicon Economy +
01/2022 completion of Version 1.0 of the Authorization Module +
04/2022 completion of Version 1.0 of the Digital Folder

|*O2*
|*Parts*
|The milestones described in O1 already address the individual modules of the blockchain broker.

|*O3*
|*Process model*
|Development, agile/Scrum. To describe the architecture arc42 is used. +
An architecture documentation structured according to this template is a key project result.

|*O4*
|*Development tools*
|Design with http://draw.io/[Draw.io], in addition Magic Draw or Enterprise Architect. +
Go and TypeScript source code created in IntelliJ

|*O5*
|*Configuration management*
|Git at GitLab hosted by Fraunhofer: link:https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager[]

|*O6*
|*Test tools and test processes*
|Unit testing as part of a CI/CD Pipeline (stage test) +
Go based components: Package testing +
Typescript based components: Testing Framework Jest

|*O7*
|*Other tools*
|Cosmos SDK v0.44.5 & Ignite CLI 0.23.0 for Blockchain Development +
Node.js 16 as JavaScript Runtime Environment +
npm 8 for Build Management +
NestJS 8 as Application Framework +
Kubernetes (OKD version 4.11) based Silicon Economy Platform
|===

== 2.3 Political Constraints

The following political constraints apply:

[cols=",,",options="header",]
|===
|Id
|Constraint
|Background and / or Motivation

|*P1*
|Release as Open Source
|The source code of the solution is made available as open source. License: Open Logistics Foundation License 1.3, Hosted by the Open Logistics Foundation.

|*P2*
|All dependencies are Open Source
|A complete list of (transitive) dependencies and their Open Source licences needs to be provided.
|===

== 2.4 Conventions

All conventions are listed in the https://oe160.iml.fraunhofer.de/wiki/display/HOW/Developer+Guidelines[Silicon Economy Development Guidelines], e.g.

[cols=",,",options="header",]
|===
|Id
|Convention
|Background and / or Motivation

|*C1*
|Architecture documentation
|Terminology and structure according to [.underline]#arc42#, version 6.0

|*C2*
|Coding guidelines for Go
|built-in linter specific conventions, checked using SonarQube

|*C3*
|Coding guidelines for TypeScript
|https://angular.io/guide/styleguide[Angular coding style] / https://github.com/google/gts[Google TypeScript Style] conventions, checked using SonarQube

|*C4*
|Language
|Documentation in English, naming of things (components, interfaces, variables) in diagrams and source code in English

|*C5*
|Versioning
|Semantic Versioning 2.0.0 (https://semver.org/lang/de/) in GitLab

|*C6*
|Specific file formats
|JSON (preferred), YAML, XML
|===

